# **What is NEO-LOADER?** #
***
NEO-LOADER is a new, powerful, fast and flexible CSV import tool for Neo4j. 

It is different from other existing loader tools for Neo4j in that it uses **model** files to describe the data, rather than requiring you to parse SQL export data into separate nodes and relationships file. Moreover it allows you to describe relationships between entities in different CSV files, and thereby compose the data for a single node from multiple sources. 

# **How does it work?** #
***
You create text-based model files that describe how you want your CSV-based data to appear in Neo4j. The model files have a straightforward syntax and are expressed using JSON.  
 
## Major features ##

 * Model files mean **no programming and no need to understand Cypher or Neo4j** internals.
 * You can load arbitrarily** large/complex CSV resources without the need for pre-processing**
 * Allows you to compose individual graph objects from **multiple CSV resources** during import
 * Performs **on-the-fly data transformations** - such as sortable timestamps from date strings.
 * Enables **conditional assignment of labels to nodes**
 * Allows **optional creation of relationships** between nodes
 * Provides **declarative strategies for handling duplicate data** : merge, version, unique
 * Automatically **handles different versions of Neo4j**

# **Supported platforms** #
***
NEO-LOADER is known to work on the following platforms:

 * Debian-based and RPM/RHEL-based Linuxes
 * Mac OS/X

There is currently no support for Windows. However if you are on a Windows platform, you can install a guest VM from one of the supported platforms and run NEO-LOADER inside the virtual machine.

# **Try NEO-LOADER** #
***
NEO-LOADER comes with a small demo example that you can run "out of the box". The tutorials on the Wiki are also based on this simple demo, which covers most of the features. The demo will import a small 'satellites' database into your existing Neo4j server. It won't delete any existing databases you already have.

First, ensure you have exported a NEO4J_HOME environment variable to your shell that points to your Neo4j install path. 

> For Linux users, Neo4j is installed by default in ```/var/lib/neo4j```. For Mac OS/X users, there is no default
install location, but if you installed using the brew package manager (recommended), NEO4J_HOME should be set to ```/usr/local/Cellar/neo4j/<version>/libexec```

After you have set the NEO4J_HOME environment variable, download the source code and run the demo:

```
$ git clone https://vbickers@bitbucket.org/vbickers/neo-loader.git
$ cd neo-loader
$ bin/neo-loader demo
```
# **Licensing** #
***
NEO-LOADER is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

NEO-LOADER is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with NEO-LOADER. If not, see <http://www.gnu.org/licenses/>.

# **Feedback** #
***
For general comments and requests, send an email to:
> <mailto:vince@mambofish.org>

To learn more visit the project wiki:
> <http://bitbucket.org/vbickers/neo-loader/wiki/browse/>

For bug reports, visit the NEO-LOADER issues page:
> <http://bitbucket.org/vbickers/neo-loader/issues/>