# Copyright (C) 2014 Vince Bickers
#
# This file is part of NEO-LOADER.
#
# NEO-LOADER is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# NEO-LOADER is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with NEO-LOADER. If not, see <http://www.gnu.org/licenses/>.
#

#!/bin/bash

package="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "$package/neo"

# retrieves the import database path from the neo4j.properties file
function database() {
    echo $(cat config/neo4j.properties | grep name | cut -f2 -d=)
}

# imports external data and sets up the database for first use
function import() {
    if [ -z $(neo_pid) ]
    then
        echo "Stopping Neo4j"
        neo_stop
    fi
    sudo chown -R $USER:$GROUP $(database) >/dev/null 2>&1
    nohup java -cp . -Xmx$1m -jar target/neo-loader-1.0-SNAPSHOT-jar-with-dependencies.jar > $(database).import.log 2>&1 &
    echo $! > neo-loader.pid
}

# aborts the import process
function abort() {
    kill -9 $(cat neo-loader.pid)
}

# creates the application. you need to do this at least once to build for your version of neo4j
function make() {
    if [ "$1" ]
    then
        neo4j_version="$1"
    else
        neo4j_version=$(neo_version)
    fi
    if [ -z "$neo4j_version" ]
        then
            echo "Please start Neo4j and run this command again, or specify the version of Neo4j to use, e.g. make 2.1.6"
        else
            echo "Building importer for Neo4j version: $neo4j_version"
            sed "s/<neo4j.version.*/<neo4j.version>$neo4j_version<\/neo4j.version>/" pom.xml > pom.updated
            mv pom.updated pom.xml
            mvn clean package
    fi
}

function status() {
    pid="$(cat neo-loader.pid)"
    proc="$(ps -p $pid | grep $pid)"
    if [ "$proc" ]
    then
        echo "$proc"
        echo "Running, pid: $(cat neo-loader.pid)"
    else
        echo "Not running"
    fi
}

function deploy() {
    neo_switch "$(database)"
}

function demo() {

    rm -rf data/demo
    mkdir -p data/demo

    echo "neo4j.db.path=$package/data/demo/" > config/neo4j.properties
    echo "neo4j.db.name=satellites.db" >> config/neo4j.properties

    echo "importer.models=import/satellites.json" > config/importer.properties

    make

    import 256

    neo_switch satellites.db
}

$@
