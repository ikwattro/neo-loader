/*
 * Copyright (C) 2014 Vince Bickers
 * This file is part of NEO-LOADER.
 *
 * NEO-LOADER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NEO-LOADER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NEO-LOADER. If not, see <http://www.gnu.org/licenses/>.
 *
*/

package org.mambofish.neo.loader.config;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

public abstract class BaseConfig implements Configuration {

    Configuration baseConfig;

    static Configuration readConfig(String configPropertiesFileName) {

        Configuration config = null;

        try {
            config = new PropertiesConfiguration(configPropertiesFileName);
        }  catch (ConfigurationException e) {
            System.out.println("=================================================");
            System.out.println("   STARTUP ERROR                            ");
            System.out.println("     Unable to find/read config properties file " + configPropertiesFileName);
            System.out.println("     (See stack trace below)  ");
            e.printStackTrace();
            System.out.println("=================================================");
            System.exit(1);
        }

        return config;
    }

    @Override
    public Configuration subset(String prefix) {
        return baseConfig.subset(prefix);
    }

    @Override
    public boolean isEmpty() {
        return baseConfig.isEmpty();
    }

    @Override
    public boolean containsKey(String key) {
        return baseConfig.containsKey(key);
    }

    @Override
    public void addProperty(String key, Object value) {
        baseConfig.addProperty(key, value);
    }

    @Override
    public void setProperty(String key, Object value) {
        baseConfig.setProperty(key, value);
    }

    @Override
    public void clearProperty(String key) {
        baseConfig.clearProperty(key);
    }

    @Override
    public void clear() {
        baseConfig.clear();
    }

    @Override
    public Object getProperty(String key) {
        return baseConfig.getProperty(key);
    }

    @Override
    public Iterator getKeys(String prefix) {
        return baseConfig.getKeys(prefix);
    }

    @Override
    public Iterator getKeys() {
        return baseConfig.getKeys();
    }

    @Override
    public Properties getProperties(String key) {
        return baseConfig.getProperties(key);
    }

    @Override
    public boolean getBoolean(String key) {
        return baseConfig.getBoolean(key);
    }

    @Override
    public boolean getBoolean(String key, boolean defaultValue) {
        return baseConfig.getBoolean(key, defaultValue);
    }

    @Override
    public Boolean getBoolean(String key, Boolean defaultValue) {
        return baseConfig.getBoolean(key, defaultValue);
    }

    @Override
    public byte getByte(String key) {
        return baseConfig.getByte(key);
    }

    @Override
    public byte getByte(String key, byte defaultValue) {
        return baseConfig.getByte(key, defaultValue);
    }

    @Override
    public Byte getByte(String key, Byte defaultValue) {
        return baseConfig.getByte(key, defaultValue);
    }

    @Override
    public double getDouble(String key) {
        return baseConfig.getDouble(key);
    }

    @Override
    public double getDouble(String key, double defaultValue) {
        return baseConfig.getDouble(key, defaultValue);
    }

    @Override
    public Double getDouble(String key, Double defaultValue) {
        return baseConfig.getDouble(key, defaultValue);
    }

    @Override
    public float getFloat(String key) {
        return baseConfig.getFloat(key);
    }

    @Override
    public float getFloat(String key, float defaultValue) {
        return baseConfig.getFloat(key, defaultValue);
    }

    @Override
    public Float getFloat(String key, Float defaultValue) {
        return baseConfig.getFloat(key, defaultValue);
    }

    @Override
    public int getInt(String key) {
        return baseConfig.getInt(key);
    }

    @Override
    public int getInt(String key, int defaultValue) {
        return baseConfig.getInt(key, defaultValue);
    }

    @Override
    public Integer getInteger(String key, Integer defaultValue) {
        return baseConfig.getInteger(key, defaultValue);
    }

    @Override
    public long getLong(String key) {
        return baseConfig.getLong(key);
    }

    @Override
    public long getLong(String key, long defaultValue) {
        return baseConfig.getLong(key, defaultValue);
    }

    @Override
    public Long getLong(String key, Long defaultValue) {
        return baseConfig.getLong(key, defaultValue);
    }

    @Override
    public short getShort(String key) {
        return baseConfig.getShort(key);
    }

    @Override
    public short getShort(String key, short defaultValue) {
        return baseConfig.getShort(key, defaultValue);
    }

    @Override
    public Short getShort(String key, Short defaultValue) {
        return baseConfig.getShort(key, defaultValue);
    }

    @Override
    public BigDecimal getBigDecimal(String key) {
        return baseConfig.getBigDecimal(key);
    }

    @Override
    public BigDecimal getBigDecimal(String key, BigDecimal defaultValue) {
        return baseConfig.getBigDecimal(key, defaultValue);
    }

    @Override
    public BigInteger getBigInteger(String key) {
        return baseConfig.getBigInteger(key);
    }

    @Override
    public BigInteger getBigInteger(String key, BigInteger defaultValue) {
        return baseConfig.getBigInteger(key, defaultValue);
    }

    @Override
    public String getString(String key) {
        return baseConfig.getString(key);
    }

    @Override
    public String getString(String key, String defaultValue) {
        return baseConfig.getString(key, defaultValue);
    }

    @Override
    public String[] getStringArray(String key) {
        return baseConfig.getStringArray(key);
    }

    @Override
    public List getList(String key) {
        return baseConfig.getList(key);
    }

    @Override
    public List getList(String key, List defaultValue) {
        return baseConfig.getList(key, defaultValue);
    }

}
