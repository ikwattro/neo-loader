package org.mambofish.neo.loader.config;

public class BatchInserterConfig extends BaseConfig {

    private static final String CONFIG_PROPERTIES_FILE = "config/batchinserter.properties";

    public BatchInserterConfig() {
        this.baseConfig = readConfig(CONFIG_PROPERTIES_FILE);
    }

    public BatchInserterConfig(String configFile) {
        this.baseConfig = readConfig(configFile);
    }

}
