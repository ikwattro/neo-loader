package org.mambofish.neo.loader.context;

import com.google.inject.Inject;
import org.mambofish.neo.loader.importer.Importer;
import org.mambofish.neo.loader.importer.ImporterModule;

import static org.mambofish.neo.loader.context.GuiceContext.GuiceModules;

@GuiceModules(ImporterModule.class)
public class ApplicationContext extends GuiceContext {

    @Inject
    private Importer importer;

    public ApplicationContext() {
        super();
    }

    public Importer getImporter() throws Exception {
        return importer;
    }

}
