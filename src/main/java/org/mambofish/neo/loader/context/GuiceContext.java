package org.mambofish.neo.loader.context;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;

import java.lang.annotation.*;

public abstract class GuiceContext {

    @Target(ElementType.TYPE)
    @Retention(RetentionPolicy.RUNTIME)
    @Inherited
    public @interface GuiceModules {
        Class<?>[] value();
    }

    protected GuiceContext() {
        try {
            injector().injectMembers(this);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private Injector injector() throws IllegalAccessException, InstantiationException {
        Class<?>[] classes = modules();
        Module[] modules = new Module[classes.length];
        for (int i = 0; i < classes.length; i++) {
            modules[i] = (Module) (classes[i]).newInstance();
        }
        return Guice.createInjector(modules);
    }

    private Class<?>[] modules() {
        Class<?> clazz = this.getClass();
        GuiceModules annotation = clazz.getAnnotation(GuiceModules.class);
        if (annotation == null) {
            throw new RuntimeException("Missing @GuiceModules annotation '" + clazz.getName()+ "'");
        }
        return annotation.value();
    }

}
