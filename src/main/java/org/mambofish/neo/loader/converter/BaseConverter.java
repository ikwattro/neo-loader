/*
 * Copyright (C) 2014 Vince Bickers
 * This file is part of NEO-LOADER.
 *
 * NEO-LOADER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NEO-LOADER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NEO-LOADER. If not, see <http://www.gnu.org/licenses/>.
 *
*/
package org.mambofish.neo.loader.converter;

import com.google.common.collect.Lists;
import com.google.inject.Inject;
import org.mambofish.neo.loader.handler.EndpointHandler;
import org.mambofish.neo.loader.model.*;
import org.mambofish.neo.loader.parser.ModelParser;
import org.mambofish.neo.loader.resource.CSVResource;
import org.mambofish.neo.loader.resource.DataRow;

import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;

public class BaseConverter implements Converter {

    /*
     * Note that the calculation of cores double-counts on hyper-threaded CPUs.
     * In any case, we never take more than half the available cores for the active
     * thread pool so that we don't dominate the the thread scheduler.
     *
     * In practice, with the the slowness of the batch inserter (to which access must be serialised),
     * just three or four cores will be sufficient to fully utilise the capacity of the batch inserter.
     */
    private static final int CORES = Math.max(Runtime.getRuntime().availableProcessors() / 2 - 1, 1);

    /* Used to signal the RowHandler threads to terminate */
    private static final DataRow STOP = new DataRow(null);

    private final CSVResource loader;
    private final ModelParser parser;
    private final EndpointHandler handler;

    private final TypeCatalogs typeCatalogs = new TypeCatalogs();
    private final EdgeCatalog edgeCatalog = new EdgeCatalog();
    private final Map<String, TaxonIndex> taxonIndexMap = new ConcurrentHashMap<>();

    private final ProgressMonitor progressMonitor = new ProgressMonitor();

    @Inject
    public BaseConverter(CSVResource resourceLoader, ModelParser modelParser, EndpointHandler endpointHandler) {
        this.loader = resourceLoader;
        this.parser = modelParser;
        this.handler = endpointHandler;
    }

    @Override
    public Integer convert(String modelFile) throws Exception {
        Model model = parser.parse(modelFile);
        for (Resource resource : model.resources()) {
            progressMonitor.reset();
            progressMonitor.notify("Starting import of " + resource.fileName());
            importResource(resource);
        }
        return progressMonitor.done();
    }

    private void importResource(Resource resource) throws Exception {

        loader.open(resource);

        /*
         * Processing of each resource is bound to a new thread pool and blocking queue.
         * This allows the next resource to be started while the previous one is being
         * completed.
        */
        BlockingQueue<DataRow> queue = initialiseRowHandlers(resource);

        DataRow row;
        while ((row = loader.next()) != null) {
            if (row.size() == 0) {
                progressMonitor.notifyError("Skipping unparseable data: incorrect number of fields or wrong delimiter");
            } else {
                queue.put(row);
                progressMonitor.notifyProgress(1000);
            }
        }
        /* Notify the handlers to terminate*/
        queue.put(STOP);
    }

    private BlockingQueue<DataRow> initialiseRowHandlers(Resource resource) {
        BlockingQueue<DataRow> queue = new ArrayBlockingQueue<>(100000);
        for (int i = 0; i < CORES; i++) {
            new Thread(new RowHandler(queue, resource)).start();
        }
        return queue;
    }

    class RowHandler implements Runnable {

        private BlockingQueue<DataRow> queue;
        private Resource resource;

        public RowHandler(BlockingQueue queue, Resource resource) {
            this.queue = queue;
            this.resource = resource;
        }

        private void importRow(DataRow row, Resource resource) {
            List<Node> instances = Lists.newArrayList();
            for (Type type : resource.types()) {
                try {
                    if (resource.accept(type, row)) {
                        instances.add(type.newInstance(resource, row));
                    }
                } catch (InstantiationError e) {
                    progressMonitor.notifyWarning("Skipping unidentifiable instance of: " + type.name());
                }
            }
            Map<String, Object> nodes = createNodes(instances);
            createEdges(resource, row, nodes);
        }

        @Override
        public void run() {
            progressMonitor.notify("Starting new row handler: " + Thread.currentThread().getId());
            while (true) {
                try {
                    DataRow row = queue.take();
                    if (row == STOP) {
                        progressMonitor.notify("Stopping row handler: " + Thread.currentThread().getId());
                        queue.put(STOP); // push back to signal other waiting threads
                        return;
                    }
                    importRow(row, resource);
                } catch (InterruptedException ie) {
                    // we don't care.
                }
            }
        }

        private Map<String, Object> createNodes(List<Node> instances) {
            Map<String, Object> map = new HashMap<>();
            for (Node instance : instances) {
                Object node = createNode(instance);
                map.put(instance.type().name(), node);
            }
            return map;
        }

        private Object createNode(Node instance) {

            for (String taxon : instance.taxonomy()) {
                if (taxonIndexMap.get(taxon) == null) {
                    taxonIndexMap.put(taxon, handler.createTaxonIndex(taxon, instance.type().key()));
                }
            }

            UpdateStrategy updateStrategy = instance.type().updateStrategy();
            if (updateStrategy.equals(UpdateStrategy.MERGE)) {
                return updateNode(instance);
            }

            if (updateStrategy.equals(UpdateStrategy.VERSION)) {
                return insertNode(instance);
            }

            if (updateStrategy.equals(UpdateStrategy.UNIQUE)) {
                return createUniqueNode(instance);
            }

            return null;
        }

        private Object createUniqueNode(Node instance) {
            if (typeCatalogs.entries(instance).isEmpty()) {
                return insertNode(instance);
            }
            return null;  // todo: should we throw an exception here, or issue a warning?
        }

        private Object insertNode(Node instance) {
            Object node;
            node = handler.createNode(instance.properties(), instance.taxonomy());
            typeCatalogs.entries(instance).add(node);
            return node;

        }

        private void createEdges(Resource resource, DataRow row, Map<String, Object> nodes) {
            List<Edge> edges = resource.links();
            for (Edge edge : edges) {
                if (resource.accept(edge, row)) {
                    createEdge(nodes, edge);
                }
            }
        }

        private void createEdge(Map<String, Object> nodeCatalog, Edge edge) {
            Object src = nodeCatalog.get(edge.source());
            Object tgt = nodeCatalog.get(edge.target());
            if (src != null && tgt != null) {
                String label = edge.name();
                if (edgeCatalog.add(src, tgt, label)) {
                    handler.createEdge(src, tgt, label);
                }
            }
        }

        private Object updateNode(Node instance) {
            Set<Object> nodeList = typeCatalogs.entries(instance);
            Object node;
            if (nodeList.isEmpty()) {
                node = handler.createNode(instance.properties(), instance.taxonomy());
                nodeList.add(node);
            } else {
                synchronized (nodeList) {
                    node = nodeList.iterator().next();    // assume only 1 for now!
                }
                handler.updateNode(node, instance.properties());
            }
            return node;
        }

    }
}
