/*
 * Copyright (C) 2014 Vince Bickers
 * This file is part of NEO-LOADER.
 *
 * NEO-LOADER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NEO-LOADER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NEO-LOADER. If not, see <http://www.gnu.org/licenses/>.
 *
*/
package org.mambofish.neo.loader.converter;

class ProgressMonitor {

    private int processedRowCount = 0;
    private int resourceRowCount = 0;
    private int processErrorCount = 0;
    private int totalRowCount = 0;

    Integer total() {
        return totalRowCount;
    }

    public void notifyWarning(String message) {
        echo(message + " at row " + resourceRowCount);
    }

    public void notify(String message) {
        echo(message);
    }

    public void notifyError(String message) {
        processErrorCount++;
        resourceRowCount++;
        totalRowCount++;

        echo(message + " at row " + resourceRowCount);

    }

    public void notifyProgress(int modulus) {
        processedRowCount++;
        resourceRowCount++;
        totalRowCount++;
        if (processedRowCount % modulus == 0) {
            echo("loaded: " + processedRowCount + "/" + resourceRowCount + " rejected: " + processErrorCount);
        }

    }

    public Integer done() {
        echo("All done. total: " + processedRowCount + "/" + resourceRowCount + " rejected: " + processErrorCount);
        return total();

    }

    public void reset() {
        resourceRowCount = 0;
        processedRowCount = 0;
        processErrorCount = 0;
    }

    private void echo(String msg) {
        System.out.println(msg);
    }

}
