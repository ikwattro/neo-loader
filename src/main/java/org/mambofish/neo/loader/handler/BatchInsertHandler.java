/*
 * Copyright (C) 2014 Vince Bickers
 * This file is part of NEO-LOADER.
 *
 * NEO-LOADER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NEO-LOADER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NEO-LOADER. If not, see <http://www.gnu.org/licenses/>.
 *
*/
package org.mambofish.neo.loader.handler;

import org.mambofish.neo.loader.model.TaxonIndex;
import org.neo4j.graphdb.DynamicLabel;
import org.neo4j.graphdb.DynamicRelationshipType;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.schema.IndexDefinition;

import java.util.Map;

public class BatchInsertHandler extends EmbeddedNeo4JHandler implements EndpointHandler {

    public BatchInsertHandler() {
        super();
    }

    @Override
    public synchronized Object createNode() {
        return inserter().createNode(null);
    }

    @Override
    public synchronized Object createNode(Map<String, Object> properties) {
        return inserter().createNode(properties);
    }

    @Override
    public synchronized Object createNode(Map<String, Object> properties, String... categories) {
        Label[] labels = new Label[categories.length];
        for (int i = 0; i < categories.length; i++) {
            labels[i] = DynamicLabel.label(categories[i]);
        }
        return inserter().createNode(properties, labels);
    }

    @Override
    public synchronized Object createEdge(Object u, Object v) {
        return inserter().createRelationship((Long) u, (Long) v, null, null);
    }

    @Override
    public synchronized Object createEdge(Object u, Object v, String label) {
        return inserter().createRelationship((Long) u, (Long) v, DynamicRelationshipType.withName(label.toUpperCase()), null);
    }

    @Override
    public synchronized void updateNode(Object node, Map<String, Object> properties) {
        properties.putAll(inserter().getNodeProperties((Long) node));
        inserter().setNodeProperties((Long) node, properties);
    }

    @Override
    public synchronized TaxonIndex createTaxonIndex(String taxon, String property) {
        IndexDefinition indexDefinition = inserter().createDeferredSchemaIndex(DynamicLabel.label(taxon)).on(property).create();
        return new TaxonIndex(indexDefinition.getLabel().name(), indexDefinition.getPropertyKeys().iterator().next());
    }
}
