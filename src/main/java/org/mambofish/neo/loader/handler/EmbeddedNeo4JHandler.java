/*
 * Copyright (C) 2014 Vince Bickers
 * This file is part of NEO-LOADER.
 *
 * NEO-LOADER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NEO-LOADER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NEO-LOADER. If not, see <http://www.gnu.org/licenses/>.
 *
*/
package org.mambofish.neo.loader.handler;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.io.FileUtils;
import org.mambofish.neo.loader.config.BatchInserterConfig;
import org.mambofish.neo.loader.config.Neo4JConfig;
import org.neo4j.helpers.collection.MapUtil;
import org.neo4j.unsafe.batchinsert.BatchInserter;
import org.neo4j.unsafe.batchinsert.BatchInserters;

import java.io.File;
import java.io.IOException;

public abstract class EmbeddedNeo4JHandler {

    private BatchInserter batchInserter;

    private Configuration config = null;
    private Configuration neo4jConfig = null;

    EmbeddedNeo4JHandler() {
        echo("Initialising graph database...");
        if (config().getBoolean("neo4j.db.purge")) {
            purge();
        }
    }

    Configuration config() {
        if (config == null) {
            config = new BatchInserterConfig();
        }
        return config;
    }

    Configuration neo4jConfig() {
        if (neo4jConfig == null) {
            neo4jConfig = new Neo4JConfig();
        }
        return neo4jConfig;
    }

    public synchronized BatchInserter inserter() {
        if (batchInserter == null) {

            batchInserter = BatchInserters.inserter(neo4jConfig().getString("neo4j.db.path").concat(neo4jConfig().getString("neo4j.db.name")),
                    MapUtil.stringMap("cache_type", "weak",
                    "neostore.nodestore.db.mapped_memory", config().getString("neostore.nodestore.db.mapped_memory"),
                    "neostore.relationshipstore.db.mapped_memory", config().getString("neostore.relationshipstore.db.mapped_memory"),
                    "neostore.propertystore.db.mapped_memory", config().getString("neostore.propertystore.db.mapped_memory"),
                    "neostore.propertystore.db.strings.mapped_memory", config().getString("neostore.propertystore.db.strings.mapped_memory"),
                    "neostore.propertystore.db.arrays.mapped_memory", config().getString("neostore.propertystore.db.arrays.mapped_memory")
            ));

            Runtime.getRuntime().addShutdownHook(new Thread() {
                @Override
                public void run() {
                    batchInserter.shutdown();
                    echo("Successfully shutdown");
                }
            });
        }
        return batchInserter;
    }

    private void purge() {
        File f = new File(neo4jConfig().getString("neo4j.db.path"));
        try {
            if (f.exists()) {
                FileUtils.deleteDirectory(f);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    void echo(String msg) {
        System.out.println(msg);
    }

}