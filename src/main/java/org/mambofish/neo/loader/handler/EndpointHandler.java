/*
 * Copyright (C) 2014 Vince Bickers
 * This file is part of NEO-LOADER.
 *
 * NEO-LOADER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NEO-LOADER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NEO-LOADER. If not, see <http://www.gnu.org/licenses/>.
 *
*/
package org.mambofish.neo.loader.handler;

import org.mambofish.neo.loader.model.TaxonIndex;

import java.util.Map;

public interface EndpointHandler {


    /**
     * create an anonymous node in the graph
     * @return a new Node
     */
    Object createNode();

    /**
     * create an anonymous node with properties
     * @param properties a map of properties
     * @return  a new Node with the specified properties
     */
    Object createNode(Map<String, Object> properties);

    /**
     * create a unique node with properties, representing an instance of one or more categories or types.
     * @param properties a map of properties for the node
     * @param category  a list of categories to which this node may be assigned
     * @return a new Node with the specified properties and category memberships
     */
    Object createNode(Map<String, Object> properties, String... category);

    /**
     * create an unlabelled directed edge from node u to node v
     * @param u the from Node
     * @param v the to Node
     * @return  a new directed Edge u->v
     */
    Object createEdge(Object u, Object v);

    /**
     * create a labelled directed edge from node u to node v
     * @param u the from Node
     * @param v the to Node
     * @param label a label for the Edge
     * @return a new directed labelled edge u -[:label]-> v
     */
    Object createEdge(Object u, Object v, String label);

    /**
     * merge a node's existing property set with a new property set.
     * @param node  the node to update
     * @param properties a set of properties
     */
    void updateNode(Object node, Map<String, Object> properties);

    /**
     * create an index on the nodes in the specified taxon
     * using the specified node property as the key
     * @param taxon a category, type or label
     * @param property the node property name whose values will become members of that taxon
     */
    TaxonIndex createTaxonIndex(String taxon, String property);
}
