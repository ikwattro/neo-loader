/*
 * Copyright (C) 2014 Vince Bickers
 * This file is part of NEO-LOADER.
 *
 * NEO-LOADER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NEO-LOADER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NEO-LOADER. If not, see <http://www.gnu.org/licenses/>.
 *
*/
package org.mambofish.neo.loader.handler;

import org.mambofish.neo.loader.model.TaxonIndex;

import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public class StubHandler implements EndpointHandler {

    private AtomicLong id = new AtomicLong(0);

    @Override
    public Object createNode() {
        return id.incrementAndGet();
    }

    @Override
    public Object createNode(Map<String, Object> properties) {
        return id.incrementAndGet();
    }

    @Override
    public Object createNode(Map<String, Object> properties, String... category) {
        return id.incrementAndGet();
    }

    @Override
    public Object createEdge(Object u, Object v) {
        return id.incrementAndGet();
    }

    @Override
    public Object createEdge(Object u, Object v, String label) {
        return id.incrementAndGet();
    }

    @Override
    public void updateNode(Object node, Map<String, Object> properties) {

    }

    @Override
    public synchronized TaxonIndex createTaxonIndex(String taxon, String property) {
        return new TaxonIndex(taxon, property);
    }
}
