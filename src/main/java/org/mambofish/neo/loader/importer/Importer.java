/*
 * Copyright (C) 2014 Vince Bickers
 * This file is part of NEO-LOADER.
 *
 * NEO-LOADER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NEO-LOADER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NEO-LOADER. If not, see <http://www.gnu.org/licenses/>.
 *
*/
package org.mambofish.neo.loader.importer;

import org.mambofish.neo.loader.config.ImporterConfig;
import org.mambofish.neo.loader.converter.Converter;

import javax.inject.Inject;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class Importer {
    private static final String IMPORTER_MODELS ="importer.models";
    private static final Logger logger = Logger.getLogger(Importer.class.getName());

    private final Converter converter;

    @Inject
    public Importer(Converter converter) {
        this.converter = converter;
    }

    private String elapsed(long millis) {
        return String.format("%d hrs, %d mins, %d secs",
                TimeUnit.MILLISECONDS.toHours(millis),
                TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis))
        );
    }

    private String rate(int rows, long millis) {
        double r = rows * 1000.0 / (millis + 1.0);
        return ((int) r + " rows/sec");
    }

    void importData() throws Exception {
        String models = new ImporterConfig().getString(IMPORTER_MODELS);
        logger.info("Import started of model(s): " + models);
        int rows = 0;
        long now = -System.currentTimeMillis();
        for (String model : models.split(";")) {
            rows += converter.convert(model);
        }
        now += System.currentTimeMillis();
        logger.info("Import finished. Rows read: " + rows + ", time: " + elapsed(now) + ", rate: " + rate(rows, now));
    }

}
