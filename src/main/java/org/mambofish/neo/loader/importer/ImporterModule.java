/*
 * Copyright (C) 2014 Vince Bickers
 * This file is part of NEO-LOADER.
 *
 * NEO-LOADER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NEO-LOADER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NEO-LOADER. If not, see <http://www.gnu.org/licenses/>.
 *
*/
package org.mambofish.neo.loader.importer;

import com.google.inject.AbstractModule;
import org.mambofish.neo.loader.converter.BaseConverter;
import org.mambofish.neo.loader.converter.Converter;
import org.mambofish.neo.loader.handler.BatchInsertHandler;
import org.mambofish.neo.loader.handler.EndpointHandler;
import org.mambofish.neo.loader.parser.JSONModelParser;
import org.mambofish.neo.loader.parser.ModelParser;
import org.mambofish.neo.loader.resource.CSVResource;
import org.mambofish.neo.loader.resource.DefaultCSVResource;

public class ImporterModule extends AbstractModule {

    protected void configure() {
        bind(CSVResource.class).to(DefaultCSVResource.class);
        bind(Converter.class).to(BaseConverter.class);
        bind(EndpointHandler.class).to(BatchInsertHandler.class);
        bind(ModelParser.class).to(JSONModelParser.class);
    }
}
