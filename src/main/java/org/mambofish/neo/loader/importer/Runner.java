package org.mambofish.neo.loader.importer;

import org.mambofish.neo.loader.context.ApplicationContext;

public class Runner {

    public static void main(String[] args) throws Exception {
        new ApplicationContext().getImporter().importData();
    }

}
