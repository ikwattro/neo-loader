package org.mambofish.neo.loader.importer;

import com.google.inject.AbstractModule;
import org.mambofish.neo.loader.converter.BaseConverter;
import org.mambofish.neo.loader.converter.Converter;
import org.mambofish.neo.loader.handler.EndpointHandler;
import org.mambofish.neo.loader.handler.StubHandler;
import org.mambofish.neo.loader.parser.JSONModelParser;
import org.mambofish.neo.loader.parser.ModelParser;
import org.mambofish.neo.loader.resource.CSVResource;
import org.mambofish.neo.loader.resource.DefaultCSVResource;

public class StubModule extends AbstractModule {

    protected void configure() {
        bind(CSVResource.class).to(DefaultCSVResource.class);
        bind(Converter.class).to(BaseConverter.class);
        bind(EndpointHandler.class).to(StubHandler.class);
        bind(ModelParser.class).to(JSONModelParser.class);
    }
}
