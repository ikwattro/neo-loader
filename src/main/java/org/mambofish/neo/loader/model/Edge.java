/*
 * Copyright (C) 2014 Vince Bickers
 * This file is part of NEO-LOADER.
 *
 * NEO-LOADER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NEO-LOADER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NEO-LOADER. If not, see <http://www.gnu.org/licenses/>.
 *
*/
package org.mambofish.neo.loader.model;

import java.util.Map;

public class Edge implements Optional {

    private final Map<String, Object> properties;

    public Edge(Map<String, Object> properties) {
        this.properties = properties;
    }

    public String source() {
        return (String) properties.get("source");
    }

    public String target() {
        return (String) properties.get("target");
    }

    public String name() {
        return (String) properties.get("name");
    }

    public String condition() {
        return (String) properties.get("condition");
    }

}
