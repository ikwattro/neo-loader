/*
 * Copyright (C) 2014 Vince Bickers
 * This file is part of NEO-LOADER.
 *
 * NEO-LOADER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NEO-LOADER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NEO-LOADER. If not, see <http://www.gnu.org/licenses/>.
 *
*/
package org.mambofish.neo.loader.model;

import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * The KeyCatalog contains a list of all the keys for nodes of a specific type, e.g. Person nodes,
 * that are encountered during the data load.
 *
 * If nodes are inserted using a version strategy (e.g. instead of a merge strategy), then
 * the same node key ("ref" property) may occur on multiple nodes. For example we might
 * have two person nodes with ref='john doe'. For this reason, the keyCatalog maintains a unique
 * set of all the node references associated with each node key.
 */
class KeyCatalog {

    private final Map<String, Set<Object>> catalog = new ConcurrentHashMap<>();

    public synchronized Set<Object> entries(String key) {
        //key = normalise(key);
        Set<Object> entries = catalog.get(key);
        if (entries == null) {
            entries = Collections.newSetFromMap(new ConcurrentHashMap<Object, Boolean>());
            catalog.put(key, entries);
        }
        return entries;
    }

    private String normalise(String key) {
        return key.trim().toUpperCase().replaceAll(" ", "").replaceAll(",", "").replaceAll("'", "");
    }

}
