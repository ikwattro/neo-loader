/*
 * Copyright (C) 2014 Vince Bickers
 * This file is part of NEO-LOADER.
 *
 * NEO-LOADER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NEO-LOADER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NEO-LOADER. If not, see <http://www.gnu.org/licenses/>.
 *
*/
package org.mambofish.neo.loader.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Model {

    private final List<Resource> resources = new ArrayList<>();

    public Model(Iterable<Map<String, Object>>  jsonArray) {
        for (Map<String, Object> map : jsonArray) {
            resources.add(new Resource(map));
        }
    }

    public List<Resource> resources() {
        return resources;
    }

}
