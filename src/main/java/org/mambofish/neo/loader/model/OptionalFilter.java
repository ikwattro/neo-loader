/*
 * Copyright (C) 2014 Vince Bickers
 * This file is part of NEO-LOADER.
 *
 * NEO-LOADER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NEO-LOADER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NEO-LOADER. If not, see <http://www.gnu.org/licenses/>.
 *
*/
package org.mambofish.neo.loader.model;

import org.mambofish.neo.loader.resource.DataRow;

import java.util.Map;

class OptionalFilter {

    private final Iterable<Map<String, Object>> conditionMappings;

    public OptionalFilter(Iterable<Map<String, Object>> conditionMappings) {
        this.conditionMappings = conditionMappings;
    }

    /**
     *
     * A condition is an equality expression like
     *
     *  "type-indicator=FRP"
     *
     * The left hand side is a condition variable that must be declared in the
     * conditionMappings section of the JSON model
     *
     * The right hand is a data value which the condition accepts
     *
     * The ConditionMappings map the condition variable to a column in the current row of data
     *
     * @param optional the instance of optional to test
     * @param row the row supplying the data values for comparison
     * @return whether or not the condition is met by the data in the relevant column
     */
    public boolean accept(Optional optional, DataRow row) {

        String condition = optional.condition();

        // unconditionally accept
        if (condition == null) {
            return true;
        }

        String[] conditionParts = condition.split("=");
        String key = conditionParts[0];
        Object expectedValue = conditionParts[1];

        for (Map<String, Object> map : conditionMappings) {
            if (map.get("name").equals(key)) {
                Object value = row.get((String) map.get("column"));
                if (value != null && value.equals(expectedValue)) {
                    return true;
                }
            }
        }
        return false;
    }

}
