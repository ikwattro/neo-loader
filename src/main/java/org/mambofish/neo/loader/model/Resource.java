/*
 * Copyright (C) 2014 Vince Bickers
 * This file is part of NEO-LOADER.
 *
 * NEO-LOADER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NEO-LOADER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NEO-LOADER. If not, see <http://www.gnu.org/licenses/>.
 *
*/
package org.mambofish.neo.loader.model;

import org.mambofish.neo.loader.resource.DataRow;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Resource {

    private final Map<String, Object> properties;

    private final List<Edge> edges = new ArrayList<>();
    private final List<Type> types = new ArrayList<>();
    private final OptionalFilter entityFilter;

    @SuppressWarnings("unchecked")
    public Resource(Map<String, Object> properties) {
        this.properties = properties;
        for (Map<String, Object> map : (Iterable<Map<String, Object>>) properties.get("edges")) {
            edges.add(new Edge(map));
        }
        for (Map<String, Object> map : (Iterable<Map<String, Object>>) properties.get("labels")) {
            types.add(new Type(map));
        }
        entityFilter = new OptionalFilter((Iterable<Map<String, Object>>) properties.get("conditionMappings"));
    }

    public String fileName() {
        return (String) properties.get("resource");
    }

    public List<Type> types() {
        return types;
    }

    public List<Edge> links() {
        return edges;
    }

    @SuppressWarnings("unchecked")
    public String[] columns() {
        if (properties.get("columns") == null) {
            return null;
        }
        List<String> list = (List<String>) properties.get("columns");
        return list.toArray(new String[list.size()]);
    }

    public String delimiter() {
        return (String) properties.get("delimiter");
    }

    public boolean accept(Optional optional, DataRow row) {
        return entityFilter.accept(optional, row);
    }
}
