/*
 * Copyright (C) 2014 Vince Bickers
 * This file is part of NEO-LOADER.
 *
 * NEO-LOADER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NEO-LOADER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NEO-LOADER. If not, see <http://www.gnu.org/licenses/>.
 *
*/
package org.mambofish.neo.loader.model;

import java.util.Map;

/**
 * A Taxon permits a set of nodes to be grouped together under a unique classifying name, and is persisted
 * into the graph as a distinct entity that can be used in queries.
 *
 * Every node belongs to at least one taxon (the base taxon) which is obtained from its Type, but it may
 * additionally (and optionally) be classified in different taxa as well.
 *
 * For example, a node 'John' is assigned a Type 'Person'. This type becomes the base taxon for that
 * node. However, John may also belong to the taxon Employee - depending on the semantics of the data being loaded.
 *
 * Apart from the root taxon, which is mandatory, all additional taxa a node belongs may be optionally defined.
 *
 * Taxa are strictly orthogonal, that is a node's specific taxonomy does not imply any classification hierarchy
 * even when such a hierarchy might be inferred.
 *
 */
public class Taxon implements Optional {

    private final Map<String, Object> properties;

    public Taxon(Map<String, Object> properties) {
        this.properties = properties;
    }

    public String name() {
        return (String) properties.get("name");
    }

    public String condition() {
        return (String) properties.get("condition");
    }

}
