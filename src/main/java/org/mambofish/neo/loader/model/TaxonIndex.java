/*
 * Copyright (C) 2014 Vince Bickers
 * This file is part of NEO-LOADER.
 *
 * NEO-LOADER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NEO-LOADER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NEO-LOADER. If not, see <http://www.gnu.org/licenses/>.
 *
*/
package org.mambofish.neo.loader.model;

import java.util.ArrayList;
import java.util.List;

/**
 * A TaxonIndex defines an abstraction over a specific database's indexing capabilities of Taxa (node groups).
 *
 * Every implementation of EndpointHandler is required to return a TaxonIndex when the method createTaxonIndex()
 * is invoked, however different implementations will vary in the capabilities that they can provide from
 * such an object. For example, the Neo4j handler is unable to provide the set of node objects in the index because it
 * defers creation of indices until the handler is shutdown.
 *
 */
public class TaxonIndex {

    private final String taxonomy;
    private final String property;
    private final List<Object> nodes;

    public TaxonIndex(String taxon, String property) {
        this.taxonomy = taxon;
        this.property = property;
        this.nodes = new ArrayList<>();
    }

    public String getTaxonomy() {
        return taxonomy;
    }

    public String getProperty() {
        return property;
    }

    public List<Object> getNodes() {
        return nodes;
    }
}
