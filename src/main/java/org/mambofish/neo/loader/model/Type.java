/*
 * Copyright (C) 2014 Vince Bickers
 * This file is part of NEO-LOADER.
 *
 * NEO-LOADER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NEO-LOADER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NEO-LOADER. If not, see <http://www.gnu.org/licenses/>.
 *
*/
package org.mambofish.neo.loader.model;

import org.mambofish.neo.loader.resource.DataRow;
import org.mambofish.neo.loader.transformer.GenderTransform;
import org.mambofish.neo.loader.transformer.PostcodeTransformer;
import org.mambofish.neo.loader.transformer.SortableDateTransformer;
import org.mambofish.neo.loader.transformer.Transformer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Types are used to uniquely classify Nodes, e.g. John is a Person.
 *
 * Usually, a node's Type is synonymous with its base Taxon, however, in the case where an edge
 * must be created between two instances of a Person, (for example John -knows-> Jane), John and Jane must be able to
 * be uniquely looked up by their Type, which is not possible if they are both assigned to the type Person.
 *
 * To handle this scenario, John and Jane require an index attribute on their Type (e.g. Person.1, Person.2)
 * The Type indexes are used when creating edges between John and Jane, but are ignored when assigning John and
 * Jane to their root Taxon, that is they will be both belong to the same Taxon "Person".
 */
public class Type implements Optional {

    private static final Map<String, Transformer> transformers = new HashMap<>();

    static {
        transformers.put("sortable_date", new SortableDateTransformer());
        transformers.put("postcode", new PostcodeTransformer());
        transformers.put("gender", new GenderTransform());
    }

    private final Map<String, Object> properties;
    private final Map<String, Class> conversions = new HashMap<>();
    private final List<Mapping> mappings = new ArrayList<>();
    private final List<Taxon> taxa = new ArrayList<>();
    private String key = "ref"; // the default key property for all nodes

    @SuppressWarnings("unchecked")
    public Type(Map<String, Object> properties) {

        this.properties = properties;

        for (Map<String, Object> map : (Iterable<Map<String, Object>>) properties.get("mappings")) {
            mappings.add(new Mapping(map));
        }

        if (properties.get("taxonomy") != null) {
            for (Map<String, Object> map : (Iterable<Map<String, Object>>) properties.get("taxonomy")) {
                taxa.add(new Taxon(map));
            }
        }

        if (properties.get("key") != null) {
            this.key = (String) properties.get("key");
        }

    }

    public String name() {
        return (String) properties.get("name");
    }

    public UpdateStrategy updateStrategy() {
        return UpdateStrategy.valueOf(((String) properties.get("update_strategy")).toUpperCase());
    }


    public String condition() {
        return (String) properties.get("condition");
    }

    @SuppressWarnings("unchecked")
    public List<String> identifiers() {
        return (List<String>) properties.get("identifiers");
    }

    List<Mapping> mappings() {
        return mappings;
    }

    public List<Taxon> taxa() {
        return taxa;
    }

    public String key() {
        return this.key;
    }

    public Node newInstance(Resource resource, DataRow row) throws InstantiationError {

        Map<String, Object> properties = new HashMap<>();
        List<String> taxonomy = new ArrayList<>();

        properties.put(this.key(), createIdentity(row));
        properties.put("updated", System.currentTimeMillis());


        for (Mapping mapping : mappings()) {
            Object value = value(row, mapping);
            if (value != null) {
                properties.put(mapping.name(), value);
            }
        }

        taxonomy.add(baseName());

        for (Taxon taxon : taxa) {
            if (resource.accept(taxon, row)) {
                taxonomy.add(taxon.name());
            }
        }
        return new Node(this, properties, taxonomy.toArray(new String[taxonomy.size()]));
    }

    public String baseName() {
        int p = name().indexOf(".");
        if (p > -1) {
            return name().substring(0, p);
        }
        return name();
    }

    private Object value(DataRow row, Mapping mapping) {

        String columnName = mapping.column();
        Object rawData = row.get(columnName);

        if (rawData == null) {
            return null;
        }

        String value = rawData.toString().replace("\"", "").trim();

        Class converter = conversions.get(columnName);

        if (converter != null) {
            try {
                if (converter == Integer.class) return Integer.parseInt(value);
                if (converter == Long.class) return Long.parseLong(value);
                if (converter == Double.class) return Double.parseDouble(value);
                return value.replace("'", "\\'");
            } catch (NumberFormatException nfe) {
                // indicates inconsistent data types found, e.g. int, then double. we reset the converter (fall through)
            }
        }
        Object converted = toStringOrNumber(value);
        if (converted != null) {
            conversions.put(columnName, converted.getClass());
        }
        return converted;

    }

    private String transform(String transformation, Object rawValue) {

        int colon = transformation.indexOf(":");
        String transformer = transformation.substring(0, colon);
        String format = transformation.substring(colon + 1);

        return transformers.get(transformer).transform(rawValue, format);
    }

    public static Object toStringOrNumber(String value) {

        if (value == null || value.length() == 0) {
            return null;
        }

        try {
            return Integer.parseInt(value);
        } catch (Exception ie) {
            try {
                return Long.parseLong(value);
            } catch (Exception le) {
                try {
                    return Double.parseDouble(value);
                } catch (Exception de) {
                    return value.replace("'", "\\'");
                }
            }
        }
    }


    private String createIdentity(DataRow row) throws InstantiationError {

        StringBuilder sb = new StringBuilder();

        if (identifiers() == null || identifiers().isEmpty()) {
            throw new InstantiationError("no identifier(s) defined");
        }

        for (String identifier : identifiers()) {
            for (Mapping mapping : mappings()) {
                String f = mapping.name();
                if (f.equals(identifier)) {
                    sb.append(value(row, mapping));
                    sb.append(" ");
                }
            }
        }

        String identity = sb.toString().trim();
        if (identity.length() == 0) {
            throw new InstantiationError("identifier(s) not declared in node attribute mappings");
        }
        return identity;
    }

}
