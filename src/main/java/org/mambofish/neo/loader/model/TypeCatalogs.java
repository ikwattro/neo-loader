/*
 * Copyright (C) 2014 Vince Bickers
 * This file is part of NEO-LOADER.
 *
 * NEO-LOADER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NEO-LOADER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NEO-LOADER. If not, see <http://www.gnu.org/licenses/>.
 *
*/
package org.mambofish.neo.loader.model;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * The TypeCatalog contains, for each node Type, its associated KeyCatalog.
 */
public class TypeCatalogs {

    private final Map<String, KeyCatalog> map = new ConcurrentHashMap<>();

    KeyCatalog getCatalog(Type type) {
        KeyCatalog catalog = map.get(type.baseName());
        if (catalog == null) {
            catalog = new KeyCatalog();
            /// must use the root taxon based on the type name
            map.put(type.baseName(), catalog);
        }
        return catalog;
    }

    public Set<Object> entries(Node instance) {
        return getCatalog(instance.type()).entries(instance.id());
    }

    public Set<String> types() {
        return map.keySet();
    }

}
