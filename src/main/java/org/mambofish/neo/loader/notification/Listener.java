/*
 * Copyright (C) 2014 Vince Bickers
 * This file is part of NEO-LOADER.
 *
 * NEO-LOADER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NEO-LOADER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NEO-LOADER. If not, see <http://www.gnu.org/licenses/>.
 *
*/package org.mambofish.neo.loader.notification;

import org.mambofish.neo.loader.model.Node;

public interface Listener {
    void notifyInsert(Object node, Node instance);
    void notifyAlias(Object node, Node instance, Object alias);
    void notifyEdge(Object a, Object b, String label);
    void notifyUpdate(Object node, Node instance);
    void notifyWarning(String msg);
}
