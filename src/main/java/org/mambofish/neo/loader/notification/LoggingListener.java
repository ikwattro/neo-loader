/*
 * Copyright (C) 2014 Vince Bickers
 * This file is part of NEO-LOADER.
 *
 * NEO-LOADER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NEO-LOADER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NEO-LOADER. If not, see <http://www.gnu.org/licenses/>.
 *
*/package org.mambofish.neo.loader.notification;

import org.mambofish.neo.loader.model.Node;

import java.util.logging.Logger;

public class LoggingListener implements Listener {

    private static final Logger logger = Logger.getLogger(LoggingListener.class.getName());

    public void notifyInsert(Object node, Node instance) {
        logger.fine("inserted node[" + node + "] (" + instance.type().name() + ":" + instance.id() + ")");
    }

    public void notifyAlias(Object node, Node instance, Object alias) {
        logger.fine("linking node  " + node + ": (" + instance.id() + ") <--> " + "(" + alias + ")");
    }

    public void notifyEdge(Object a, Object b, String label) {
        logger.fine("created edge (:" + a + ") -[:" + label + "]-> (:" + b + ")");
    }

    public void notifyUpdate(Object node, Node instance) {
        logger.fine("updated node[" + node + "]: (" + instance.type().name() + ":" + instance.id() + ")");
    }

    public void notifyWarning(String msg) {
        logger.warning(msg);
    }


}
