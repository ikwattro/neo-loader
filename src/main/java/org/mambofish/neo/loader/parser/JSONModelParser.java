/*
 * Copyright (C) 2014 Vince Bickers
 * This file is part of NEO-LOADER.
 *
 * NEO-LOADER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NEO-LOADER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NEO-LOADER. If not, see <http://www.gnu.org/licenses/>.
 *
*/package org.mambofish.neo.loader.parser;


import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import org.apache.commons.io.IOUtils;
import org.mambofish.neo.loader.model.Model;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class JSONModelParser implements ModelParser {

    @SuppressWarnings("unchecked")
    @Override
    public Model parse(String modelFile) throws IOException {

        try (InputStream is = new FileInputStream( modelFile )) {
            return new Model(((JSONObject) JSONSerializer.toJSON(IOUtils.toString( is ))).getJSONArray("resources"));
        }
        catch (IOException e) {
            throw new IOException(e);
        }
        catch (NullPointerException npe) {
            throw new RuntimeException("Not found: " + modelFile);
        }
    }

}
