/*
 * Copyright (C) 2014 Vince Bickers
 * This file is part of NEO-LOADER.
 *
 * NEO-LOADER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NEO-LOADER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NEO-LOADER. If not, see <http://www.gnu.org/licenses/>.
 *
*/
package org.mambofish.neo.loader.resource;

import java.util.Map;

public class DataRow {

    private final Map<String, Object> fields;

    public DataRow(Map<String, Object> fields) {
        this.fields = fields;
    }

    public Object get(String column) {
        return fields.get(column);
    }

    public int size() {
        return fields.size();
    }

    public String toString() {

        StringBuilder builder = new StringBuilder();
        for (Map.Entry entry : fields.entrySet()) {
            builder.append(entry.getKey()).append(": ").append(entry.getValue()).append(" ");
        }
        return builder.toString();
    }
}
