/*
 * Copyright (C) 2014 Vince Bickers
 * This file is part of NEO-LOADER.
 *
 * NEO-LOADER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NEO-LOADER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NEO-LOADER. If not, see <http://www.gnu.org/licenses/>.
 *
*/package org.mambofish.neo.loader.resource;

import org.mambofish.neo.loader.model.Resource;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Scanner;

public class DefaultCSVResource implements CSVResource {

    private Scanner scanner;
    private String[] header;
    private String delimiter;

    @Override
    public void open(Resource resource) throws IOException {

        InputStream is = new FileInputStream(resource.fileName());

        // if we have already got a scanner open, close it first
        if (scanner != null) {
            scanner.close();
        }

        scanner = new Scanner(is);

        // fetch optionally defined column names. if undefined, expect them
        // to be the first line of the csv file.
        header = resource.columns();
        if (header == null) {
            fetchHeaderRow();
        }

        // optionally specify the delimiter, defaults to tab if not set
        delimiter = resource.delimiter();
        if (delimiter == null) {
            delimiter = "\t";
        }

    }

    private void fetchHeaderRow() throws IOException {
        if (scanner.hasNext()) {
            header = scanner.nextLine().split(",");
            for (int i = 0; i < header.length; i++) {
                String h = header[i];
                header[i] = h.replace("\"","").replace("'","");
            }
        } else {
            scanner.close();
            throw new IOException("No columns defined in csv file");
        }
    }

    @Override
    public DataRow next() {

        if (scanner.hasNext()) {

            String line = scanner.nextLine();
            // need to determin if split returns natural object type. int, long, float, etc.
            Object[] data = line.split(delimiter, header.length);
            HashMap<String, Object> map = new HashMap<>();

            for (int i = 0; i < header.length; i++ ) {
                map.put(header[i], data[i]);
            }

            return new DataRow(map);
        } else {
            scanner.close();
            return null;
        }
    }
}
