/*
 * Copyright (C) 2014 Vince Bickers
 * This file is part of NEO-LOADER.
 *
 * NEO-LOADER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NEO-LOADER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NEO-LOADER. If not, see <http://www.gnu.org/licenses/>.
 *
*/
package org.mambofish.neo.loader.transformer;

public class PostcodeTransformer implements Transformer {


    @Override
    public String transform(Object data, String format) {

        if (data == null) {
            return "?";
        }
        String src = data.toString();
        if (src.length() < 6) {
            return "?" + src;
        }
        String lastPart = src.substring(src.length()-3);
        String firstPart = src.substring(0, src.length()-3).trim();

        return firstPart.concat(" ").concat(lastPart);
    }
}
