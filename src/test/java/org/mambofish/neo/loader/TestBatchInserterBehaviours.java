package org.mambofish.neo.loader;

import org.junit.Test;
import org.mambofish.neo.loader.handler.BatchInsertHandler;
import org.mambofish.neo.loader.handler.TestBatchInserter;
import org.mambofish.neo.loader.model.TaxonIndex;
import org.neo4j.graphdb.Label;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class TestBatchInserterBehaviours {

    private static final BatchInsertHandler handler = new TestBatchInserter();

    /**
     * when identities are merged, so are their properties
     */
    @Test
    public void propertyMerge() {

        Map<String, Object> p1 = map("p1:p1,p2:p2");
        Map<String, Object> p2 = map("p2:p2,p3:p3");

        Long node = (Long) handler.createNode(p1);
        handler.updateNode(node, p2);

        Map<String, Object> union = handler.inserter().getNodeProperties(node);

        assertNotNull("p3", union.get("p3"));
        assertNotNull("p2", union.get("p2"));
        assertNotNull("p1", union.get("p1"));

    }

    /**
     * a node can have multiple types (neo4j labels)
     */
    @Test
    public void typeAliases() {

        Long node = (Long) handler.createNode(map("id:mir"), "Satellite", "SpaceStation");

        int expected = 0;
        int count = 0;

        for (Label label : handler.inserter().getNodeLabels(node)) {
            count++;
            if (label.name().equals("Satellite")) expected++;
            if (label.name().equals("SpaceStation")) expected++;
        }

        assertEquals(2, count);
        assertEquals(expected, count);
    }


    /**
     * node types (neo4j labels) are automatically indexed
     */
    @Test
    public void autoIndexing() {

        handler.createNode(map("id:node1"), "Person", "Employee");
        handler.createNode(map("id:node2"), "Person", "Employer");

        TaxonIndex personIndex = handler.createTaxonIndex("Person", "ref");
        TaxonIndex employeeIndex = handler.createTaxonIndex("Employee", "ref");
        TaxonIndex employerIndex = handler.createTaxonIndex("Employer", "ref");

        // this cannot be tested in a generic way, because the IndexCreator interface in neo4j does
        // not expose a method listing the nodes that have been/ will be indexed (understandably so).
        // so we just assert as much as we can here. If these tests pass, we can assume that neo4j
        // will do its thing properly

        assertEquals("Person", personIndex.getTaxonomy());
        assertEquals("Employee", employeeIndex.getTaxonomy());
        assertEquals("Employer", employerIndex.getTaxonomy());

        assertEquals("ref", personIndex.getProperty());
        assertEquals("ref", employeeIndex.getProperty());
        assertEquals("ref", employerIndex.getProperty());

    }

    private static Map<String, Object> map(String mappings) {
        Map<String, Object> map = new HashMap<>();
        if (mappings != null) {
            String[] elements = mappings.split(",");
            for (String element : elements) {
                String[] pair = element.split(":");
                map.put(pair[0], pair[1]);
            }
        }
        return map;
    }
}
