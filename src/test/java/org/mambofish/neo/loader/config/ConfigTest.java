package org.mambofish.neo.loader.config;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class ConfigTest {

    private BatchInserterConfig batchInserterConfig;
    private ImporterConfig importerConfig;
    private Neo4JConfig neo4JConfig;

    @Before
    public void setUp() {
        batchInserterConfig = new BatchInserterConfig();
        importerConfig = new ImporterConfig();
        neo4JConfig = new Neo4JConfig();
    }

    @Test
    public void testNeo4jConfig() {
        assertNotNull(neo4JConfig.getString("neo4j.db.path"));
    }

    @Test
    public void testImporterConfig() {
        assertNotNull(importerConfig.getString("importer.models"));
    }

    @Test
    public void testBatchInserterConfig() {
        assertNotNull(batchInserterConfig.getBoolean("neo4j.db.purge"));
        assertNotNull(batchInserterConfig.getString("neostore.nodestore.db.mapped_memory"));
        assertNotNull(batchInserterConfig.getString("neostore.relationshipstore.db.mapped_memory"));
        assertNotNull(batchInserterConfig.getString("neostore.propertystore.db.mapped_memory"));
        assertNotNull(batchInserterConfig.getString("neostore.propertystore.db.strings.mapped_memory"));
        assertNotNull(batchInserterConfig.getString("neostore.propertystore.db.arrays.mapped_memory"));
    }
}
