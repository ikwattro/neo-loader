package org.mambofish.neo.loader.handler;

import org.apache.commons.configuration.Configuration;
import org.mambofish.neo.loader.config.BatchInserterConfig;
import org.mambofish.neo.loader.config.Neo4JConfig;

public class TestBatchInserter extends BatchInsertHandler {

    public Configuration config() {
        return new BatchInserterConfig("src/test/resources/config/batchinserter.test.properties");
    }

    public Configuration neo4jConfig() {
        return new Neo4JConfig("src/test/resources/config/neo4j.test.properties");
    }
}
