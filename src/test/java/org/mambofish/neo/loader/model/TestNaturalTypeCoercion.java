package org.mambofish.neo.loader.model;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class TestNaturalTypeCoercion {

    @Test
    public void testIntegerString() {
        Integer o = 12345;
        String s = "12345";
        assertEquals(o, Type.toStringOrNumber(s));

    }

    @Test
    public void testLongString() {
        Long o = 112233445566778899L;
        String s = "112233445566778899";
        assertEquals(o, Type.toStringOrNumber(s));
    }

    @Test
    public void testDoubleString() {
        Double o = 0.00000021d;
        String s = "0.00000021d";
        assertEquals(o, Type.toStringOrNumber(s));
    }

}
