package org.mambofish.neo.loader.model;

import org.junit.Test;
import org.mambofish.neo.loader.parser.JSONModelParser;
import org.mambofish.neo.loader.resource.DataRow;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static junit.framework.Assert.assertEquals;

public class TestType {

    private static final JSONModelParser parser = new JSONModelParser();
    private static final String DRIVERS = "src/test/resources/imports/drivers-model.json";
    private static final String SATELLITES = "src/test/resources/imports/satellites.json";

    @Test
    public void validate() throws Exception {
        Type address = parser.parse(DRIVERS).resources().get(0).types().get(0);
        assertEquals("Address", address.name());
        assertEquals("house_number", address.identifiers().get(0));
        assertEquals("post_code", address.identifiers().get(1));
        assertEquals(UpdateStrategy.MERGE, address.updateStrategy());
    }

    @Test
    public void satelliteTaxonomy() throws Exception {
        Resource resource = parser.parse(SATELLITES).resources().get(0);
        Type satellite = resource.types().get(0);
        List<Taxon> taxa = satellite.taxa();
        assertEquals(1, taxa.size());
        assertEquals("SpaceStation", taxa.get(0).name());
        assertEquals("manned_indicator=Y", taxa.get(0).condition());
    }

    @Test
    public void testUnmannedSatelliteIsNotASpaceStation() throws Exception {
        Resource resource = parser.parse(SATELLITES).resources().get(0);
        Type satellite = resource.types().get(0);
        DataRow row = new DataRow(map("Object:Sputnik 1,Orbit:Elliptical,Alt:LEO,Program:Soviet,Manned:N,Launched:04 Oct 1957,Status:0"));

        Node sputnik = satellite.newInstance(resource, row);

        assertEquals(1, sputnik.taxonomy().length);
        assertEquals("Satellite", sputnik.taxonomy()[0]);

    }

    @Test
    public void testMannedSatelliteIsASpaceStation() throws Exception {

        Resource resource = parser.parse(SATELLITES).resources().get(0);
        Type satellite = resource.types().get(0);

        DataRow row = new DataRow(map("Object:Mir,Orbit:Elliptical,Alt:LEO,Program:Soviet,Manned:Y,Launched:04 Oct 1957,Status:0"));

        Node mir = satellite.newInstance(resource, row);

        assertEquals(2, mir.taxonomy().length);
        assertEquals("Satellite", mir.taxonomy()[0]);
        assertEquals("SpaceStation", mir.taxonomy()[1]);

    }

    private static Map<String, Object> map(String mappings) {
        Map<String, Object> map = new HashMap<>();
        if (mappings != null) {
            String[] elements = mappings.split(",");
            for (String element : elements) {
                String[] pair = element.split(":");
                map.put(pair[0], pair[1]);
            }
        }
        return map;
    }

}
