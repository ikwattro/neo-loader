package org.mambofish.neo.loader.parser;

import org.junit.Before;
import org.junit.Test;
import org.mambofish.neo.loader.model.Model;

import java.io.IOException;

import static junit.framework.Assert.assertEquals;

public class TestModelParser {

    private Model model;

    @Before
    public void setUp() throws IOException {
        model = new JSONModelParser().parse("src/test/resources/imports/drivers-model.json");
    }

    @Test
    public void testResource() {
        assertEquals(1, model.resources().size());
    }

}
