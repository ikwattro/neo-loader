package org.mambofish.neo.loader.resource;

import org.junit.Before;
import org.junit.Test;
import org.mambofish.neo.loader.model.*;
import org.mambofish.neo.loader.parser.JSONModelParser;

import java.io.IOException;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

public class TestMultiResourceModel {

    private CSVResource resourceLoader;
    private Model model;

    @Before
    public void setUp() throws IOException {
        model = new JSONModelParser().parse("src/test/resources/imports/multi-resource.json");
        resourceLoader = new DefaultCSVResource();
    }

    @Test
    public void testTwoResources() {
        assertEquals(2, model.resources().size());
    }

    @Test
    public void testDonationsResource() throws Exception {
        Resource donations = model.resources().get(0);
        resourceLoader.open(donations);

        Type subject_sponsor = donations.types().get(0);
        Type object_fundraiser = donations.types().get(1);

        Type subject_fundraiser = donations.types().get(2);
        Type object_charity = donations.types().get(3);

        assertEquals("Sponsor", subject_sponsor.name());
        assertEquals("Fundraiser", object_fundraiser.name());
        assertEquals("Fundraiser", subject_fundraiser.name());
        assertEquals("Charity", object_charity.name());

        assertEquals("type_indicator=SP", subject_sponsor.condition());
        assertEquals("type_indicator=SP", object_fundraiser.condition());
        assertEquals("type_indicator=FR", subject_fundraiser.condition());
        assertEquals("type_indicator=FR", object_charity.condition());


        // michal;FR;CRUK
        DataRow first = resourceLoader.next();

        assertTrue(donations.accept(subject_fundraiser, first));
        assertTrue(donations.accept(object_charity, first));

        Node fundraiser = subject_fundraiser.newInstance(donations, first);
        Node charity = object_charity.newInstance(donations, first);

        assertEquals(2, fundraiser.taxonomy().length);
        assertEquals(1, charity.taxonomy().length);

        // fundraisers are persons
        assertEquals("Fundraiser", fundraiser.taxonomy()[0]);
        assertEquals("Person", fundraiser.taxonomy()[1]);

        // charities are just charities
        assertEquals("Charity", charity.taxonomy()[0]);

        // vince;SP;Michal
        DataRow next = resourceLoader.next();

        assertTrue(donations.accept(subject_sponsor, next));
        assertTrue(donations.accept(object_fundraiser, next));

        Node sponsor = subject_sponsor.newInstance(donations,next);
        fundraiser = object_fundraiser.newInstance(donations,next);

        assertEquals(2, fundraiser.taxonomy().length);
        assertEquals(2, sponsor.taxonomy().length);

        // fundraisers are persons
        assertEquals("Fundraiser", fundraiser.taxonomy()[0]);
        assertEquals("Person", fundraiser.taxonomy()[1]);

        // so are sponsors
        assertEquals("Sponsor", sponsor.taxonomy()[0]);
        assertEquals("Person", sponsor.taxonomy()[1]);

    }

    @Test
    public void testFriendships() throws Exception {
        Resource friends = model.resources().get(1);
        resourceLoader.open(friends);

        Type subject_person = friends.types().get(0);
        Type object_person = friends.types().get(1);

        // Michal;Vince
        DataRow next = resourceLoader.next();

        assertTrue(friends.accept(subject_person, next));
        assertTrue(friends.accept(object_person, next));

        Node p1 = subject_person.newInstance(friends,next);
        Node p2 = object_person.newInstance(friends,next);

        assertEquals("Michal", p1.id());
        assertEquals("Vince", p2.id());

        // ensure we can uniquely identify multiple instances of the same type
        // (needed when defining edges between nodes)
        assertEquals("Person.1", p1.type().name());
        assertEquals("Person.2", p2.type().name());

        // and that the type index is not included in the taxonomy
        assertEquals("Person", p1.taxonomy()[0]);
        assertEquals("Person", p2.taxonomy()[0]);




    }
}
