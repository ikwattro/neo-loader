package org.mambofish.neo.loader.resource;

import org.junit.Before;
import org.junit.Test;
import org.mambofish.neo.loader.model.Model;
import org.mambofish.neo.loader.model.Resource;
import org.mambofish.neo.loader.parser.JSONModelParser;

import java.io.IOException;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNull;
import static org.junit.Assert.assertNotNull;

public class TestResource {

    private Resource resource;

    @Before
    public void setUp() throws IOException {
        Model model = new JSONModelParser().parse("src/test/resources/imports/drivers-model.json");
        resource = model.resources().get(0);
    }

    @Test
    public void testFileName() {
        assertEquals("src/test/resources/imports/drivers.csv", resource.fileName());
    }

    @Test
    public void testDelimiter() {
        assertEquals(",", resource.delimiter());
    }

    @Test
    public void testLinks() {
        assertNotNull(resource.links());
    }

    @Test
    public void testColumns() {
        assertNull(resource.columns());
    }

}
