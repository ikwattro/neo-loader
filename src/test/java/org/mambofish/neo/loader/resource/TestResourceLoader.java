package org.mambofish.neo.loader.resource;

import org.junit.Before;
import org.junit.Test;
import org.mambofish.neo.loader.model.Model;
import org.mambofish.neo.loader.parser.JSONModelParser;

import java.io.IOException;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNull;

public class TestResourceLoader {

    private CSVResource resourceLoader;
    private Model model;

    @Before
    public void setUp() throws IOException {
        model = new JSONModelParser().parse("src/test/resources/imports/drivers-model.json");
        resourceLoader = new DefaultCSVResource();
        resourceLoader.open(model.resources().get(0));
    }

    @Test
    public void testReadAllData() throws IOException {
        int i = 0;
        while (resourceLoader.next() != null) ++i;
        assertEquals(7, i);
    }

    @Test
    public void testFieldWithSpaces() throws IOException {
        DataRow row = resourceLoader.next();
        assertEquals(row.get("HOUSE_NUMBER"), "The Black House");
    }

    @Test
    public void testNumberOfFields() throws Exception {
        assertEquals(5, resourceLoader.next().size());
    }

    @Test
    public void testThatSameDataLoaderCanOpenNewResources() throws IOException {
        // scan the file line by line
        while(resourceLoader.next() != null);

        // open a new resource (or the same one again) and read all the records
        resourceLoader.open(model.resources().get(0));
        for (int i = 0; i < 7; i++, resourceLoader.next());

        // eof
        assertNull(resourceLoader.next());


    }


}
